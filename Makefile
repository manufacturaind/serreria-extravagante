FONT_NAME = MFI-Serreria-Extravagante

default: build

# simple local build
build:
	. `pwd`/.env/bin/activate; fib convert $(FONT_NAME).sfd --ttf --otf
	fontimage $(FONT_NAME).ttf -o $(FONT_NAME).png
	zip $(FONT_NAME).zip *.ttf *.png LICENSE README.md

# for use within gitlab's CI environment
ci:
	fib convert $(FONT_NAME).sfd --ttf --otf
	fontimage $(FONT_NAME).ttf -o $(FONT_NAME).png
	zip $(FONT_NAME).zip *.ttf *.png *.otf LICENSE README.md
	# rm -f *.ttf *.png *.otf

install:
	virtualenv .env --system-site-packages
	. `pwd`/.env/bin/activate; pip install git+https://gitlab.com/foundry-in-a-box/fib.git

clean:
	rm -f *.ttf *.png *.otf *.zip
